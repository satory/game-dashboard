/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var dashboardApp = angular.module('dashboardApp', ['ui.router', 'dashboardFactory', 'dashboardControl']);

dashboardApp.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/overview');

        // States
        $stateProvider
                .state('overview', {
                    url: '/overview',
                    templateUrl: 'templates/overview.html',
                    controller: 'OverviewCtrl',
                    resolve: {
                        overview: ['overview', function (overview) {
                                return overview.query()();
                            }]
                    }
                })
                .state('playerList', {
                    url: "/playerList",
                    templateUrl: 'templates/playerList.html',
                    controller: 'PlayerListCtrl',
                    resolve: {
                        playerList: ['player', function (player) {
                                return player.findAll();
                            }]
                    }
                })
                .state('player', {
                    url: "/player/:playerId",
                    templateUrl: 'templates/player.html',
                    controller: 'PlayerCtrl',
                    resolve: {
                        player: ['player', '$stateParams'
                                    , function (player, $stateParams) {
                                        return player.get({playerId: $stateParams.playerId});
                                    }]
                    }
                })
                .state('teamList', {
                    url: "/teamList",
                    templateUrl: 'templates/teamList.html',
                    controller: 'TeamListCtrl',
                    resolve: {
                        teamList: ['team', function (team) {
                                return team.findAll();
                            }]
                    }
                })
                .state('team', {
                    url: "/team/:teamId",
                    templateUrl: 'templates/team.html',
                    controller: 'TeamCtrl',
                    resolve: {
                        team: ['team', '$stateParams'
                                    , function (team, $stateParams) {
                                        return team.get({teamId: $stateParams.teamId});
                                    }]
                    }
                })
                .state('competitionList', {
                    url: "/competitionList",
                    templateUrl: 'templates/competitionList.html',
                    controller: 'CompetitionListCtrl',
                    resolve: {
                        competitionList: ['competition', function (competition) {
                                return competition.findAll();
                            }]
                    }
                })
                .state('competition', {
                    url: "/competition/:competitionId",
                    templateUrl: 'templates/competition.html',
                    controller: 'CompetitionCtrl',
                    resolve: {
                        competition: ['competition', '$stateParams'
                                    , function (competition, $stateParams) {
                                        return competition.get({competitionId: $stateParams.competitionId});
                                    }]
                    }
                });
    }
]).run(function($state) {
  $state.go('overview'); //make a transition to overview state when app starts
});
