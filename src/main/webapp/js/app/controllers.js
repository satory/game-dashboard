var dashboardControllers = angular.module('dashboardControl', ['dashboardFactory']);

dashboardControllers.controller('PlayerListCtrl', ['$scope', 'playerList', function ($scope, playerList) {
        $scope.playerList = playerList;
    }]);
dashboardControllers.controller('PlayerCtrl', ['$scope', 'player', function ($scope, player) {
        $scope.player = player;
    }]);
dashboardControllers.controller('TeamListCtrl', ['$scope', 'teamList', function ($scope, teamList) {
        $scope.teamList = teamList;
    }]);
dashboardControllers.controller('TeamCtrl', ['$scope', 'team', function ($scope, team) {
        $scope.team = team;
    }]);
dashboardControllers.controller('CompetitionListCtrl', ['$scope', 'competitionList', function ($scope, competitionList) {
        $scope.competitionList = competitionList;
    }]);
dashboardControllers.controller('CompetitionCtrl', ['$scope', 'competition', function ($scope, competition) {
        $scope.competition = competition;
    }]);
dashboardControllers.controller('OverviewCtrl', ['$scope', 'overview', function ($scope, overview) {
        $scope.overview = overview;
    }]);

dashboardControllers.controller('FixtureCreateController', function($scope, $state, $stateParams, competition) {
  $scope.fixture = new Fixture();  //create new movie instance. Properties will be set via ng-model on UI
 competition.postFixture($scope.fixture);
  
      $state.go('competition'); // on success go back to competition page again...
});