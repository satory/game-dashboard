/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.dto;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author john
 */
@XmlRootElement(name = "overview")
public class OverviewDto implements Serializable {
    @XmlElement
    private Integer numberOfGamesPlayed;
    @XmlElement
    private Integer totalNumberOfGames;
    @XmlElement
    private Integer numberOfPlayers;
    @XmlElement
    private Integer numberOfCompetitions;
    @XmlElement
    private List<PlayerOverviewDto> playerOverview;

    public Integer getNumberOfGamesPlayed() {
        return numberOfGamesPlayed;
    }

    public void setNumberOfGamesPlayed(Integer numberOfGamesPlayed) {
        this.numberOfGamesPlayed = numberOfGamesPlayed;
    }

    public Integer getTotalNumberOfGames() {
        return totalNumberOfGames;
    }

    public void setTotalNumberOfGames(Integer totalNumberOfGames) {
        this.totalNumberOfGames = totalNumberOfGames;
    }

    public Integer getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public void setNumberOfPlayers(Integer numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
    }

    public Integer getNumberOfCompetitions() {
        return numberOfCompetitions;
    }

    public void setNumberOfCompetitions(Integer numberOfCompetitions) {
        this.numberOfCompetitions = numberOfCompetitions;
    }

    public List<PlayerOverviewDto> getPlayerOverview() {
        return playerOverview;
    }

    public void setPlayerOverview(List<PlayerOverviewDto> playerOverview) {
        this.playerOverview = playerOverview;
    }

    @Override
    public String toString() {
        return "OverviewDto{" + "numberOfGamesPlayed=" + numberOfGamesPlayed + ", totalNumberOfGames=" + totalNumberOfGames + ", numberOfPlayers=" + numberOfPlayers + ", numberOfCompetitions=" + numberOfCompetitions + ", playerOverview=" + playerOverview + '}';
    }
}