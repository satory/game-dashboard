/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.dto;

import javax.xml.bind.annotation.XmlRootElement;
import org.jmcdonnell.dashboard.beans.Player;

/**
 *
 * @author john
 */
@XmlRootElement(name = "player")
public class PlayerDto implements Dto<Player> {
    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public PlayerDto convertTo(Player entity) {
        this.id = entity.getId();
        this.name = entity.getName();
        return this;
    }

    @Override
    public Player convertFrom() {
        Player player = new Player();
        player.setId(id);
        player.setName(name);
        return player;
    }    
}
