/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import org.jmcdonnell.dashboard.beans.Competition;
import org.jmcdonnell.dashboard.beans.CompetitionFixture;
import org.jmcdonnell.dashboard.beans.CompetitionType;
import org.jmcdonnell.dashboard.beans.Player;
import org.jmcdonnell.dashboard.beans.Team;

/**
 *
 * @author john
 */
@XmlRootElement(name = "competition")
public class CompetitionDto implements Dto<Competition> {
    @XmlElement
    private Integer id;
    @XmlElement
    private String name;
    @XmlElement
    private CompetitionType competitionType;
    @XmlElementWrapper(name = "players")
    private List<PlayerDto> players;
    @XmlElementWrapper(name = "teams")
    private List<TeamDto> teams;
    @XmlElementWrapper(name = "fixtures")
    private List<FixtureDto> fixtures;

    @Override
    public CompetitionDto convertTo(Competition entity) {
        id = entity.getId();
        name = entity.getName();
        competitionType = entity.getCompetitionType();
        players = new ArrayList<>(entity.getPlayers().size());
        for (Player player : entity.getPlayers()) {
            PlayerDto dto = new PlayerDto();
            players.add(dto.convertTo(player));
        }
        teams = new ArrayList<>(entity.getTeams().size());
        for (Team team : entity.getTeams()) {
            TeamDto dto = new TeamDto();
            teams.add(dto.convertTo(team));
        }
        fixtures = new ArrayList<>(entity.getFixtures().size());
        for ( CompetitionFixture fixture : entity.getFixtures()) {
            FixtureDto dto = new FixtureDto();
            fixtures.add(dto.convertTo(fixture));
        }
        return this;
    }

    @Override
    public Competition convertFrom() {
        Competition competition = new Competition();
        competition.setId(id);
        competition.setName(name);
        Set<Player> allPlayers = new HashSet<>(players.size());
        for (PlayerDto player : players) {
            allPlayers.add(player.convertFrom());
        }
        competition.setPlayers(allPlayers);
        
        Set<Team> allTeams = new HashSet<>(teams.size());
        for (TeamDto team : teams) {
            allTeams.add(team.convertFrom());
        }
        competition.setTeams(allTeams);
        competition.setCompetitionType(competitionType);
        List<CompetitionFixture> allFixtures = new ArrayList<>(fixtures.size());
        for (FixtureDto fixture : fixtures) {
            allFixtures.add(fixture.convertFrom());
        }
        competition.setFixtures(allFixtures);
        return competition;
    }   
}
