/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author john
 */
@XmlRootElement(name = "playerOverview")
public class PlayerOverviewDto {
    private PlayerDto player;
    private Integer CompetitonsEntered;
    private Integer gamesPlayed;
    private Integer gamesWon;
    private Integer gamesLost;
    private Integer gamesDrawn;
    private Integer homeGames;
    private Integer homeGamesWon;
    private Integer homeGamesLost;
    private Integer homeGamesDrawn;
    private Integer awayGames;
    private Integer awayGamesWon;
    private Integer awayGamesLost;
    private Integer awayGamesDrawn;
}
