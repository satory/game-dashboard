/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.dto;

import javax.xml.bind.annotation.XmlRootElement;
import org.jmcdonnell.dashboard.beans.Team;

/**
 *
 * @author john
 */
@XmlRootElement(name = "team")
public class TeamDto implements Dto<Team> {
    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public TeamDto convertTo(Team entity) {
        this.id = entity.getId();
        this.name = entity.getName();
        return this;
    }

    @Override
    public Team convertFrom() {
        Team team = new Team();
        team.setId(id);
        team.setName(name);
        return team;
    }    
}