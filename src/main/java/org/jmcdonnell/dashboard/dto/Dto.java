/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.dto;

import java.io.Serializable;

/**
 *
 * @author john
 * @param <T>
 */
public interface Dto<T> extends Serializable {
    Dto convertTo(T entity);
    T convertFrom();
}
