/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.jmcdonnell.dashboard.beans.Competition;
import org.jmcdonnell.dashboard.beans.CompetitionFixture;
import org.jmcdonnell.dashboard.beans.Player;

/**
 *
 * @author john
 */
@XmlRootElement(name = "fixture")
public class FixtureDto implements Dto<CompetitionFixture> {
    @XmlElement
    private Integer id;
    @XmlElement
    private Integer homeScore;
    @XmlElement
    private Integer awayScore;
    @XmlElement
    private Competition competition;
    @XmlElement
    private Player homePlayer;
    @XmlElement
    private Player awayPlayer;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(Integer homeScore) {
        this.homeScore = homeScore;
    }

    public Integer getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(Integer awayScore) {
        this.awayScore = awayScore;
    }

    public Competition getCompetition() {
        return competition;
    }

    public void setCompetition(Competition competition) {
        this.competition = competition;
    }

    public Player getHomePlayer() {
        return homePlayer;
    }

    public void setHomePlayer(Player homePlayer) {
        this.homePlayer = homePlayer;
    }

    public Player getAwayPlayer() {
        return awayPlayer;
    }

    public void setAwayPlayer(Player awayPlayer) {
        this.awayPlayer = awayPlayer;
    }

    @Override
    public FixtureDto convertTo(CompetitionFixture entity) {
        id = entity.getId();
        homeScore =  entity.getHomeScore();
        homePlayer = entity.getHomePlayer();
        awayScore = entity.getAwayScore();
        awayPlayer = entity.getAwayPlayer();
        competition = entity.getCompetition();
        
        return this;
    }

    @Override
    public CompetitionFixture convertFrom() {
        CompetitionFixture fixture = new CompetitionFixture();
        fixture.setAwayPlayer(awayPlayer);
        fixture.setAwayScore(awayScore);
        fixture.setCompetition(competition);
        fixture.setHomePlayer(homePlayer);
        fixture.setHomeScore(homeScore);
        fixture.setId(id);
        
        return fixture;
    }
}
