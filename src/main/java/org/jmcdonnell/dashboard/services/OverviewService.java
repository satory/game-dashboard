/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.services;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import org.jboss.logging.Logger;
import org.jmcdonnell.dashboard.business.OverviewManagementBean;
import org.jmcdonnell.dashboard.dto.OverviewDto;

/**
 *
 * @author john
 */
@Stateless
@Path("overview")
public class OverviewService {
    
    private static final Logger LOGGER = Logger.getLogger(AbstractService.class);
    
    @Inject
    private OverviewManagementBean overviewManagementBean;
    
    @GET
    @Produces({"application/json"})
    public OverviewDto generateOverview() {
        OverviewDto overviewDto = overviewManagementBean.getOverviewDto();
        LOGGER.debug("Generating Overview: " + overviewDto.toString());
        System.out.println("Generating Overview: " + overviewDto.toString());
        return overviewDto;
    }
}
