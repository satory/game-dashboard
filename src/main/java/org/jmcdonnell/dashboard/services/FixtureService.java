/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.services;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import org.jmcdonnell.dashboard.beans.CompetitionFixture;
import org.jmcdonnell.dashboard.business.DefaultManagementBean;
import org.jmcdonnell.dashboard.dto.FixtureDto;

/**
 *
 * @author john
 */
@Stateless
@Path("fixture")
public class FixtureService extends AbstractService<CompetitionFixture, FixtureDto> {

    @Inject
    private DefaultManagementBean defaultManagementBean;
    
    @POST
    @Consumes({"application/json"})
    @Override
    public FixtureDto create(FixtureDto entity) {
        CompetitionFixture createdFixture = defaultManagementBean.create(entity.convertFrom());
        return entity.convertTo(createdFixture);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/json"})
    public void edit(@PathParam("id") Integer id, FixtureDto entity) {
        defaultManagementBean.edit(entity.convertFrom());
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        defaultManagementBean.remove(find(id).convertFrom());
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public FixtureDto find(@PathParam("id") Integer id) {
        CompetitionFixture find = defaultManagementBean.find(id, CompetitionFixture.class);
        FixtureDto dto = new FixtureDto();
        return dto.convertTo(find);
    }

    @GET
    @Produces({"application/json"})
    public List<FixtureDto> findAll() {
        List<CompetitionFixture> findAll = defaultManagementBean.findAll(CompetitionFixture.class);
        List<FixtureDto> findAllDto = new ArrayList<>(findAll.size());
        for (CompetitionFixture fixture : findAll) {
            FixtureDto dto = new FixtureDto();
            findAllDto.add(dto.convertTo(fixture));
        }
        return findAllDto; 
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String count() {
        return String.valueOf(defaultManagementBean.count(CompetitionFixture.class));
    }
}
