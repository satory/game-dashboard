/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.services;

import java.util.List;
import javax.inject.Inject;
import org.jboss.logging.Logger;
import org.jmcdonnell.dashboard.business.DefaultManagementBean;
import org.jmcdonnell.dashboard.dto.Dto;

/**
 *
 * @author john
 * @param <T>
 * @param <D>
 */
public abstract class AbstractService<T, D extends Dto<T>> {
    
    @Inject
    protected DefaultManagementBean defaultManagementBean;
    
    public abstract D create(D entity);

    public abstract void edit(Integer id, D entityType);

    public abstract void remove(Integer id);

    public abstract D find(Integer id);

    public abstract List<D> findAll();

    public abstract String count();
}
