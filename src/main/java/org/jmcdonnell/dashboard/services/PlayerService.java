/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.services;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import org.jmcdonnell.dashboard.beans.Player;
import org.jmcdonnell.dashboard.dto.PlayerDto;

/**
 *
 * @author john
 */
@Stateless
@Path("player")
public class PlayerService extends AbstractService<Player, PlayerDto>{
   
    
    @POST
    @Consumes({"application/xml", "application/json"})
    @Override
    public PlayerDto create(PlayerDto entity) {
        Player createdPlayer = defaultManagementBean.create(entity.convertFrom());
        return entity.convertTo(createdPlayer);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/json"})
    @Override
    public void edit(@PathParam("id") Integer id, PlayerDto entity) {
        defaultManagementBean.edit(entity.convertFrom());
    }

    @DELETE
    @Path("{id}")
    @Override
    public void remove(@PathParam("id") Integer id) {
        defaultManagementBean.remove(find(id).convertFrom());
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    @Override
    public PlayerDto find(@PathParam("id") Integer id) {
        Player find = defaultManagementBean.find(id, Player.class);
        PlayerDto dto = new PlayerDto();
        return dto.convertTo(find);
    }

    @GET
    @Produces({"application/json"})
    @Override
    public List<PlayerDto> findAll() {
        List<Player> findAll = defaultManagementBean.findAll(Player.class);
        List<PlayerDto> findAllDto = new ArrayList<>(findAll.size());
        for (Player player : findAll) {
            PlayerDto dto = new PlayerDto();
            findAllDto.add(dto.convertTo(player));
        }
        return findAllDto; 
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    @Override
    public String count() {
        return String.valueOf(defaultManagementBean.count(Player.class));
    }
}