/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.services;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import org.jmcdonnell.dashboard.beans.Team;
import org.jmcdonnell.dashboard.dto.TeamDto;

/**
 *
 * @author john
 */
@Stateless
@Path("team")
public class TeamService extends AbstractService<Team, TeamDto> {
    
    @POST
    @Consumes({"application/json"})
    public TeamDto create(TeamDto entity) {
        Team createdTeam = defaultManagementBean.create(entity.convertFrom());
        return entity.convertTo(createdTeam);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/json"})
    public void edit(@PathParam("id") Integer id, TeamDto entity) {
        defaultManagementBean.edit(entity.convertFrom());
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        defaultManagementBean.remove(find(id).convertFrom());
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public TeamDto find(@PathParam("id") Integer id) {
        Team find = defaultManagementBean.find(id, Team.class);
        TeamDto dto = new TeamDto();
        return dto.convertTo(find);
    }

    @GET
    @Produces({"application/json"})
    public List<TeamDto> findAll() {
        List<Team> findAll = defaultManagementBean.findAll(Team.class);
        List<TeamDto> findAllDto = new ArrayList<>(findAll.size());
        for (Team team : findAll) {
            TeamDto dto = new TeamDto();
            findAllDto.add(dto.convertTo(team));
        }
        return findAllDto; 
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String count() {
        return String.valueOf(defaultManagementBean.count(Team.class));
    }
}
