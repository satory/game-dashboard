/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.services;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import org.jmcdonnell.dashboard.beans.LeagueStanding;
import org.jmcdonnell.dashboard.business.LeagueStandingsManagementBean;

/**
 * Standings are differnt to the other servies
 * @author john
 */
@Stateless
@Path("standing")
public class StandingService {

    @Inject
    private LeagueStandingsManagementBean leagueStandingsManagementBean;
    
    @GET
    @Path("{competitionId}")
    @Produces({"application/json"})
    public List<LeagueStanding> findAll(@PathParam("competitionId") Integer id) {
        return leagueStandingsManagementBean.findAllForCompetition(id);
    }  
}
