/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.services;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import org.jmcdonnell.dashboard.beans.Competition;
import org.jmcdonnell.dashboard.beans.CompetitionFixture;
import org.jmcdonnell.dashboard.business.CompetitionManagementBean;
import org.jmcdonnell.dashboard.dto.CompetitionDto;
import org.jmcdonnell.dashboard.dto.FixtureDto;

/**
 *
 * @author john
 */
@Stateless
@Path("competition")
public class CompetitionService extends AbstractService<Competition, CompetitionDto> {
  
    @Inject
    private CompetitionManagementBean competitionManagementBean;
    
    @POST
    @Consumes({"application/json"})
    public CompetitionDto create(CompetitionDto entity) {
        Competition createdCompetition = defaultManagementBean.create(entity.convertFrom());
        return entity.convertTo(createdCompetition);
    }
    
    @POST
    @Path("{id}/")
    @Consumes({"application/json"})
    public FixtureDto create(@PathParam("id") Integer id, FixtureDto entity) {
        Competition competition = defaultManagementBean.find(id, Competition.class);
        CompetitionFixture convertFrom = entity.convertFrom();
        convertFrom.setCompetition(competition);
        CompetitionFixture create = defaultManagementBean.create(convertFrom);
        competition.getFixtures().add(convertFrom);
        defaultManagementBean.edit(competition);
        return entity.convertTo(create);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/json"})
    public void edit(@PathParam("id") Integer id, CompetitionDto entity) {
        defaultManagementBean.edit(entity.convertFrom());
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        defaultManagementBean.remove(find(id).convertFrom());
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public CompetitionDto find(@PathParam("id") Integer id) {
        Competition find = defaultManagementBean.find(id, Competition.class);
        CompetitionDto dto = new CompetitionDto();
        return dto.convertTo(find);
    }

    @GET
    @Produces({"application/json"})
    public List<CompetitionDto> findAll() {
         List<Competition> findAll = defaultManagementBean.findAll(Competition.class);
        List<CompetitionDto> findAllDto = new ArrayList<>(findAll.size());
        for (Competition competition : findAll) {
            CompetitionDto dto = new CompetitionDto();
            findAllDto.add(dto.convertTo(competition));
        }
        return findAllDto; 
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String count() {
        return String.valueOf(defaultManagementBean.count(Competition.class));
    }
}