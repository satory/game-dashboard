/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard;

import java.util.Set;
import javax.ws.rs.core.Application;
import org.jmcdonnell.dashboard.services.CompetitionService;
import org.jmcdonnell.dashboard.services.FixtureService;
import org.jmcdonnell.dashboard.services.OverviewService;
import org.jmcdonnell.dashboard.services.PlayerService;
import org.jmcdonnell.dashboard.services.StandingService;
import org.jmcdonnell.dashboard.services.TeamService;

/**
 *
 * @author john
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestServices(resources);
        return resources;
    }

    private void addRestServices(Set<Class<?>> resources) {
        resources.add(PlayerService.class);
        resources.add(CompetitionService.class);
        resources.add(TeamService.class);
        resources.add(FixtureService.class);
        resources.add(StandingService.class);
        resources.add(OverviewService.class);
    }
}
