/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.beans.comparator;

import java.util.Comparator;
import org.jmcdonnell.dashboard.beans.LeagueStanding;

/**
 *
 * @author john
 */
public class LeagueStandingComparator implements Comparator<LeagueStanding> {

    @Override
    public int compare(LeagueStanding o1, LeagueStanding o2) {
        Integer o1PointsDiff = o1.getPointsFor() - o1.getPointsAgainst();
        Integer o2PointsDiff = o2.getPointsFor() - o2.getPointsAgainst();
        
        if (!o1.getPoints().equals(o2.getPoints())) {
            return o1.getPoints().compareTo(o2.getPoints());
        } else if (!o1.getWon().equals(o2.getWon())) {
            return o1.getWon().compareTo(o2.getWon());
        }else if (!o1.getDrawn().equals(o2.getDrawn())) {
            return o1.getDrawn().compareTo(o2.getDrawn());
        } else {
            return o1PointsDiff.compareTo(o2PointsDiff);
        }
    }
}
