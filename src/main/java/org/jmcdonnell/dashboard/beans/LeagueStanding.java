/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.beans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author john
 */
@Entity
@Table(name = "LEAGUE_STANDING")
@XmlRootElement
public class LeagueStanding implements Serializable, IBean {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "played")
    private Integer played;
    @Basic(optional = false)
    @NotNull
    @Column(name = "won")
    private Integer won;
    @Basic(optional = false)
    @NotNull
    @Column(name = "drawn")
    private Integer drawn;
    @Basic(optional = false)
    @NotNull
    @Column(name = "lost")
    private Integer lost;
    @Basic(optional = false)
    @NotNull
    @Column(name = "points_for")
    private Integer pointsFor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "points_against")
    private Integer pointsAgainst;
    @Basic(optional = false)
    @NotNull
    @Column(name = "points")
    private Integer points;
    @JoinColumn(name = "competition_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Competition competition;
    @JoinColumn(name = "player_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Player player;

    public LeagueStanding() {
    }

    public LeagueStanding(Integer id) {
        this.id = id;
    }

    public LeagueStanding(Integer id, Integer played, Integer won, Integer drawn, Integer lost, Integer pointsFor, Integer pointsAgainst, Integer points) {
        this.id = id;
        this.played = played;
        this.won = won;
        this.drawn = drawn;
        this.lost = lost;
        this.pointsFor = pointsFor;
        this.pointsAgainst = pointsAgainst;
        this.points = points;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlayed() {
        return played;
    }

    public void setPlayed(Integer played) {
        this.played = played;
    }

    public Integer getWon() {
        return won;
    }

    public void setWon(Integer won) {
        this.won = won;
    }

    public Integer getDrawn() {
        return drawn;
    }

    public void setDrawn(Integer drawn) {
        this.drawn = drawn;
    }

    public Integer getLost() {
        return lost;
    }

    public void setLost(Integer lost) {
        this.lost = lost;
    }

    public Integer getPointsFor() {
        return pointsFor;
    }

    public void setPointsFor(Integer pointsFor) {
        this.pointsFor = pointsFor;
    }

    public Integer getPointsAgainst() {
        return pointsAgainst;
    }

    public void setPointsAgainst(Integer pointsAgainst) {
        this.pointsAgainst = pointsAgainst;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Competition getCompetition() {
        return competition;
    }

    public void setCompetition(Competition competition) {
        this.competition = competition;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public int hashCode() {
        Integer hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LeagueStanding)) {
            return false;
        }
        LeagueStanding other = (LeagueStanding) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.jmcdonnell.dashboard.beans.LeagueStanding[ id=" + id + " ]";
    }
}
