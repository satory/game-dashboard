/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.beans;

import javax.xml.bind.annotation.XmlEnum;

/**
 *
 * @author john
 */
@XmlEnum
public enum CompetitionType {
    LEAGUE,
    STANDALONE;
}
