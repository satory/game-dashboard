/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.dao;

import java.util.List;
import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.jmcdonnell.dashboard.beans.Competition;
import org.jmcdonnell.dashboard.beans.LeagueStanding;
import org.jmcdonnell.dashboard.beans.LeagueStanding_;
import org.jmcdonnell.dashboard.beans.Player;

/**
 *
 * @author john
 */
@Dependent
public class DashboardDao {

    @PersistenceContext(unitName = "DashboardPU")
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }

    public <T> T create(T entity) {
        getEntityManager().persist(entity);
        return entity;
    }

    public <T> T edit(T entity) {
        return getEntityManager().merge(entity);
    }

    public <T> void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public <T> T find(Object id, Class<T> entityClass) {
        return getEntityManager().find(entityClass, id);
    }

    public <T> List<T> findAll(Class<T> entityClass) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public <T> int count(Class<T> entityClass) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<LeagueStanding> getStandingsForCompetition(Object competitionId) {
        
        // might be a better way to do this, but for now, we will see how this goes.
        Competition competition = find(competitionId, Competition.class);
        
        CriteriaQuery<LeagueStanding> cq = getEntityManager().getCriteriaBuilder().createQuery(LeagueStanding.class);
        Root<LeagueStanding> leagueStanding = cq.from(LeagueStanding.class);
        cq.where(getEntityManager().getCriteriaBuilder().equal(leagueStanding.get(LeagueStanding_.competition), competition));

        return getEntityManager().createQuery(cq).getResultList();
    }
    
    public LeagueStanding getStandingForCompetitionAndPlayer(Object competitionId, Object playerId) {
        
        // might be a better way to do this, but for now, we will see how this goes.
        Competition competition = find(competitionId, Competition.class);
        
        Player player = find(playerId, Player.class);
        
        CriteriaQuery<LeagueStanding> cq = getEntityManager().getCriteriaBuilder().createQuery(LeagueStanding.class);
        Root<LeagueStanding> leagueStanding = cq.from(LeagueStanding.class);
        cq.where(getEntityManager().getCriteriaBuilder().equal(leagueStanding.get(LeagueStanding_.competition), competition))
                .where(getEntityManager().getCriteriaBuilder().equal(leagueStanding.get(LeagueStanding_.player), player));

        return getEntityManager().createQuery(cq).getSingleResult();
    }
}
