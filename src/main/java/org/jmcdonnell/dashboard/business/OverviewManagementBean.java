/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.business;

import java.util.List;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import org.jmcdonnell.dashboard.beans.Competition;
import org.jmcdonnell.dashboard.beans.CompetitionFixture;
import org.jmcdonnell.dashboard.beans.Player;
import org.jmcdonnell.dashboard.dto.OverviewDto;

/**
 *
 * @author john
 */
@Dependent
public class OverviewManagementBean {
    
    @Inject
    private DefaultManagementBean defaultManagementBean;

    public OverviewDto getOverviewDto() {
        OverviewDto dto = new OverviewDto();
        dto.setNumberOfCompetitions(defaultManagementBean.count(Competition.class));
        dto.setNumberOfPlayers(defaultManagementBean.count(Player.class));
        List<CompetitionFixture> allFixtures = defaultManagementBean.findAll(CompetitionFixture.class);
        int counterOfplayedFixtures = 0;
        for (CompetitionFixture fixture : allFixtures) {
            if(fixture.isPlayed()) {
               counterOfplayedFixtures++;     
            }
        }
        dto.setTotalNumberOfGames(allFixtures.size());
        dto.setNumberOfGamesPlayed(counterOfplayedFixtures);
        
        return dto;
    }
    
}
