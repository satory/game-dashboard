/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.business;

import java.util.Collections;
import java.util.List;
import javax.enterprise.context.Dependent;
import org.jmcdonnell.dashboard.beans.LeagueStanding;
import org.jmcdonnell.dashboard.beans.comparator.LeagueStandingComparator;
import org.jmcdonnell.dashboard.dao.DashboardDao;

/**
 *
 * @author john
 */
@Dependent
public class LeagueStandingsManagementBean {
    
    private static final Integer POINTS_FOR_WIN = 3;
    private static final Integer POINTS_FOR_DRAW = 1;
    
    private DashboardDao dashboardDao;
    
    public List<LeagueStanding> findAllForCompetition(Integer competitionId) {
        List<LeagueStanding> standingsForCompetition = dashboardDao.getStandingsForCompetition(competitionId);
        Collections.sort(standingsForCompetition, new LeagueStandingComparator());
        return standingsForCompetition;
    }
    
    public void addToStanding(Integer competitionId, Integer playerId, Boolean isWin, 
                Boolean isLost, Boolean isDraw, Integer pointsFor, Integer pointsAgainst){
        LeagueStanding standing = dashboardDao.getStandingForCompetitionAndPlayer(competitionId, playerId);
        
        if (isWin) {
            standing.setWon(standing.getWon() + 1);
            standing.setPoints(standing.getPoints() + POINTS_FOR_WIN);
        } else if (isDraw) {
            standing.setDrawn(standing.getDrawn() + 1);
            standing.setPoints(standing.getPoints() + POINTS_FOR_DRAW);
        } else {
            standing.setLost(standing.getLost() + 1);
        }
        
        standing.setPlayed(standing.getPlayed());
        standing.setPointsFor(standing.getPointsFor() + pointsFor);
        standing.setPointsAgainst(standing.getPointsAgainst()+ pointsAgainst);
        
        dashboardDao.edit(standing);
    }
}