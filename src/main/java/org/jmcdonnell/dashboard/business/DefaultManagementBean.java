/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.dashboard.business;

import java.util.List;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import org.jmcdonnell.dashboard.beans.IBean;
import org.jmcdonnell.dashboard.dao.DashboardDao;

/**
 *
 * @author john
 */
@Dependent
public class DefaultManagementBean {
    
    @Inject
    private DashboardDao dashboardDao;
    
    public<T extends IBean> T create(T entity) {
        return dashboardDao.create(entity);
    }

    public<T extends IBean> void edit(T entity) {
        dashboardDao.edit(entity);
    }

    public<T extends IBean> void remove(T entity) {
        dashboardDao.remove(dashboardDao.edit(entity));
    }

    public<T extends IBean> T find(Object id, Class<T> entityClass) {
        return dashboardDao.find(id, entityClass);
    }

    public<T extends IBean> List<T> findAll(Class<T> entityClass) {
        return dashboardDao.findAll(entityClass);
    }

    public<T extends IBean> int count(Class<T> entityClass) {
        return dashboardDao.count(entityClass);
    }
}
