DROP TABLE COMPETITION_FIXTURE;
DROP TABLE LEAGUE_STANDING;
DROP TABLE COMPETITION;
DROP TABLE COMPETITION_TYPE;
DROP TABLE PLAYER;
DROP TABLE TEAM;
DROP TABLE COMPETITION_PLAYERS;
DROP TABLE COMPETITION_TEAMS;

CREATE TABLE PLAYER (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR (256),
    PRIMARY KEY (id));

CREATE TABLE TEAM (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR (256),
    PRIMARY KEY (id));


CREATE TABLE COMPETITION_TYPE (
    type VARCHAR(256) NOT NULL,
    PRIMARY KEY (type));

CREATE TABLE COMPETITION_PLAYERS (
    competition_id INT UNSIGNED NOT NULL,
    player_id INT UNSIGNED NOT NULL,
    PRIMARY KEY (competition_id, player_id));

CREATE TABLE COMPETITION_TEAMS (
    competition_id INT UNSIGNED NOT NULL,
    team_id INT UNSIGNED NOT NULL,
    PRIMARY KEY (competition_id, team_id));

CREATE TABLE COMPETITION (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(256),
    competition_type VARCHAR(256) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (competition_type) REFERENCES COMPETITION_TYPE(type));

CREATE TABLE COMPETITION_FIXTURE (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    competition_id INT UNSIGNED NOT NULL,
    home_player_id INT UNSIGNED NOT NULL,
    home_score INT,
    away_player_id  INT UNSIGNED NOT NULL,
    away_score INT,
    PRIMARY KEY (id),
    FOREIGN KEY (competition_id) REFERENCES COMPETITION(id) ON DELETE CASCADE,
    FOREIGN KEY (home_player_id) REFERENCES PLAYER(id) ON DELETE CASCADE,
    FOREIGN KEY (away_player_id) REFERENCES PLAYER(id) ON DELETE CASCADE);

CREATE TABLE LEAGUE_STANDING (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    competition_id INT UNSIGNED NOT NULL,
    player_id INT UNSIGNED NOT NULL,
    played INT NOT NULL,
    won INT NOT NULL,
    drawn INT NOT NULL,
    lost INT NOT NULL,
    points_for INT NOT NULL,
    points_against INT NOT NULL,
    points INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (competition_id) REFERENCES COMPETITION(id) ON DELETE CASCADE,
    FOREIGN KEY (player_id) REFERENCES PLAYER(id) ON DELETE CASCADE);    

INSERT INTO COMPETITION_TYPE (type) VALUES ('LEAGUE');
INSERT INTO COMPETITION_TYPE (type) VALUES ('STANDALONE');
-- Players
INSERT INTO PLAYER(name) VALUES ('John');
INSERT INTO PLAYER(name) VALUES ('Tommy');
INSERT INTO PLAYER(name) VALUES ('Mark');
-- Teams
INSERT INTO TEAM(name) VALUES ('Atlanta Hawks');
INSERT INTO TEAM(name) VALUES ('Washington Wizards');
INSERT INTO TEAM(name) VALUES ('Toronto Raptors');
INSERT INTO TEAM(name) VALUES ('Chicago Bulls');
INSERT INTO TEAM(name) VALUES ('Milwaukee Bucks');
INSERT INTO TEAM(name) VALUES ('Cleveland Cavaliers');
INSERT INTO TEAM(name) VALUES ('Miami Heat');
INSERT INTO TEAM(name) VALUES ('Brooklyn Nets');
INSERT INTO TEAM(name) VALUES ('Charlotte Hornets');
INSERT INTO TEAM(name) VALUES ('Detroit Pistons');
INSERT INTO TEAM(name) VALUES ('Indiana Pacers');
INSERT INTO TEAM(name) VALUES ('Orlando Magic');
INSERT INTO TEAM(name) VALUES ('Boston Celtics');
INSERT INTO TEAM(name) VALUES ('Philadelphia 76ers');
INSERT INTO TEAM(name) VALUES ('New York Knicks');
INSERT INTO TEAM(name) VALUES ('Golden State Warriors');
INSERT INTO TEAM(name) VALUES ('Portland Trail Blazers');
INSERT INTO TEAM(name) VALUES ('Memphis Grizzlies');
INSERT INTO TEAM(name) VALUES ('Houston Rockets');
INSERT INTO TEAM(name) VALUES ('Dallas Mavericks');
INSERT INTO TEAM(name) VALUES ('Los Angeles Clippers');
INSERT INTO TEAM(name) VALUES ('San Antonio Spurs');
INSERT INTO TEAM(name) VALUES ('Phoenix Suns');
INSERT INTO TEAM(name) VALUES ('New Orleans Pelicans');
INSERT INTO TEAM(name) VALUES ('Oklahoma City Thunder');
INSERT INTO TEAM(name) VALUES ('Denver Nuggets');
INSERT INTO TEAM(name) VALUES ('Sacramento Kings');
INSERT INTO TEAM(name) VALUES ('Utah Jazz');
INSERT INTO TEAM(name) VALUES ('Los Angeles Lakers');
INSERT INTO TEAM(name) VALUES ('Minnesota Timberwolves');

-- Competition
INSERT INTO COMPETITION(name, competition_type) VALUES ('FRIENDLIES','STANDALONE');
INSERT INTO COMPETITION_PLAYERS(competition_id, player_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from PLAYER where name = 'John'));
INSERT INTO COMPETITION_PLAYERS(competition_id, player_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from PLAYER where name = 'Tommy'));
INSERT INTO COMPETITION_PLAYERS(competition_id, player_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from PLAYER where name = 'Mark'));

INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Atlanta Hawks'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Washington Wizards'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Toronto Raptors'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Chicago Bulls'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Milwaukee Bucks'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Cleveland Cavaliers'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Miami Heat'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Brooklyn Nets'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Charlotte Hornets'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Detroit Pistons'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Indiana Pacers'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Orlando Magic'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Boston Celtics'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Philadelphia 76ers'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'New York Knicks'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Golden State Warriors'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Portland Trail Blazers'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Memphis Grizzlies'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Houston Rockets'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Dallas Mavericks'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Los Angeles Clippers'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'San Antonio Spurs'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Phoenix Suns'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'New Orleans Pelicans'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Oklahoma City Thunder'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Denver Nuggets'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Sacramento Kings'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Utah Jazz'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Los Angeles Lakers'));
INSERT INTO COMPETITION_TEAMS(competition_id, team_id) VALUES ((SELECT id from COMPETITION where name = 'FRIENDLIES'), (SELECT id from TEAM where name = 'Minnesota Timberwolves'));